const express= require('express');
const path=require('path');
const http=require('http');
const bodyParser=require('body-parser');
var socket=require('socket.io');

const app=express();

const api = require('./routes/api');
var img,czas;


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


app.use(express.static(path.join(__dirname, 'dist')));

app.use('/api', api);

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});

app.get('/obecne',(req,res)=>{
  let dupa={
    dupa: img,
    data: czas
  }
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(dupa));
});




const server = http.createServer(app);
const port = process.env.PORT || '3000';
server.listen(port,()=>{
  console.log("Serwer ruszyl...");
});

const io=socket.listen(server);

io.on('connection',(soc)=>{
  console.log("Zaszlo");
  soc.on('sub',(room)=>{
    console.log(`Dolaczono do pokoju ${room}`);
    soc.join(room);
  });
  soc.on("img",(res)=>{
    let data=new Date();
    console.log(`${data.getHours()}:${data.getMinutes()}: IMG`);
    img='data:image/jpeg;base64,' +res.img;
    czas=`Godzina: ${data.getHours()}:${data.getMinutes()}`
  });

  app.post('/parametr',(req,res)=>{
    let obiekt={
      jaki: req.body.jaki,
      ile: req.body.ile
    }
    console.log(obiekt);
     soc.broadcast.to('123').emit('parametr',obiekt);
     res.send(200);
  });

  app.post('/flash',(req,res)=>{
    let obiekt={
      flash: req.body.flash
    }
    console.log(obiekt);
     soc.broadcast.to('123').emit('flash',obiekt);
     res.send(200);
  });

});
